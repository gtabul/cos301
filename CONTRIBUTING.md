# Contributing
--------------

Add your name to the contributors list below in the format:
 
* `<Firstname> <Lastname> - <Affiliation>`

## Contributors

* Arné Schreuder - Compiax (Pty) Ltd.
* Bernhard Schuld - Compiax (Pty) Ltd.
* Jason van Hattum - Compiax (Pty) Ltd.
* Keegan Ferrett - Compiax (Pty) Ltd.
* Gilad Tabul - University of Pretoria