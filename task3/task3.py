import argparse

# Parameter Parsing Set Up
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--primes', help='Prime Numbers', type=int, nargs='+')

# Store args
args = parser.parse_args()

def isPrime(n):
    if n == 1:
        return False
    elif n == 2:
        return True
    else:
        for i in range(2,n):
            if n % i == 0:
                return False
    return True

def determinePrimes(prime):
    n = 0
    m = 0

    for i in range(0, prime):
        if (isPrime(i) and isPrime(prime - i)):
            n = prime - i
            m = i

    print(str(n) + " + " + str(m) + " = " + str(prime))
            

    # Example output
    # 2 + 3 = 5
    # 2 + 5 = 7

if __name__ == '__main__':
    for p in args.primes:
        determinePrimes(p)